package com.orderbird.teamday;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloSpringController {

    @GetMapping("/")
    public String index() {
        return "Hello Spring!";
    }

    @GetMapping("/helloworld")
    public String helloWorld() {
        HelloWorld hello = new HelloWorld();
        return hello.helloWorld();
    }

}