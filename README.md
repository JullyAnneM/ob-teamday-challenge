[![codebeat badge](https://codebeat.co/badges/fba90195-14ab-4b69-b89f-6e9616afff87)](https://codebeat.co/projects/gitlab-com-jullyannem-ob-teamday-challenge-master)
[![pipeline status](https://gitlab.com/JullyAnneM/ob-teamday-challenge/badges/master/pipeline.svg)](https://gitlab.com/JullyAnneM/ob-teamday-challenge/commits/master)

# Orderbird Tech Challenge

This is a Tech Challenge for Orderbird. The purpose of this challenge is to fix a Java and Spring Boot project that has errors and create a Dockerfile defining how to deploy this application and run it as a container.


# Spring Boot Project

## Table of Contents

- [About the Project](#about-the-project)
- [Technologies](#technologies)
- [API Reference](#api-reference)
- [Getting Started](#getting-started)
  - [Requirements](#requirements)
  - [Installation](#installation)
  - [Running the application](#running-the-application)
  - [Running the containerized application](#running-the-containerized-application)
  - [Testing Endpoint](#testing-endpoint)
- [Extras](#extras)
  - [Makefile](#makefile)
  - [GitlabCI Pipeline](#gitlabci-pipeline)
- [Contributing](#contributing)
- [Notes](#notes)

## About the Project 

The challenge consists in fixing errors that are in this Java Project and create a Dockerfile to run the application as a container. The application should be acessible in `localhost:8080/`.

Also, it is considered a plus to develop a Makefile with rules that abstract many steps that bring this solution to life as well as a clear and concise documentation explaining how to run it.

As part of the solution, which consists of the project fixed as well as the multi-stage build Dockerfile specification, it was also developed a Continuous Integration Pipeline with GitlabCI to run everytime a commit is made to the code, integrating with Code Climate to generate a quality analysis of the project and pushing the docker image to [DockerHub](https://hub.docker.com/r/jullyannem/spring-boot-project).

## Technologies

The platforms, technologies and frameworks which were used at the time of implementation of this project are listed below: 

- Java 8  
- Gradle 3.3
- Spring framework 1.5.2
- Docker 18.09.1
- Makefile 4.2.1
- Gitlab CI 12.5.0
- Codeclimate 0.85.5
- Codebeat 

## API Reference

The developed and available RESTful API Services can be found in this section.

|   HTTP METHOD    |    Endpoint   |          Action           |
|:----------------:|:-------------:|:-------------------------:|
|       GET        |       /       |  returns "Hello Spring!"  |
|       GET        |  /helloworld  |  returns "Hello World!"   |

## Getting Started 

### Requirements 

To run the application as a container, it is important to have at least Docker 17.05 or higher installed. Otherwise, Java 8+ installed is necessary by the time you try this project out.

### Installation

First, checkout the project from this repository running:
   
```
git clone https://gitlab.com/JullyAnneM/ob-teamday-challenge.git
```

All of the command executions below should be executed from the `spring-boot-project` directory unless specified otherwise. 

To clean the directory and build the code:
   
```
./gradlew clean build
```

This command will generate the jar file inside `$(pwd)/build/libs`. 

### Running the application

You can either run the application with the jar file: 

```
PROJ_JAR=$(pwd)/build/libs
java -jar $PROJ_JAR/spring-boot-project-1.0-SNAPSHOT.jar
```

Or alternatively:

```
./gradlew run
``` 

And you will have the Spring Boot project up and running.

### Running the containerized application

You can either create the image from the Dockerfile and run the containerized application with: 

```
PROJ_REPO=$(pwd)
docker build -t spring-boot-project $PROJ_REPO
docker run -d -p 8080:8080 spring-boot-project
```

Or alternatively, you can download it from [DockerHub](https://hub.docker.com/r/jullyannem/spring-boot-project):

```
docker pull jullyannem/spring-boot-project 
docker run -d -p 8080:8080 jullyannem/spring-boot-project
```

And you will have the Spring Boot project up and running.

> **Note:** This project's Dockerfile has multi-stage builds to generate an end result image which is smaller, less complex and containing only the jar file. 

### Testing Endpoint

To check if the application is running and responding properly, you can either access `http://localhost:8080/` from the browser or execute `curl http://localhost:8080/` in the command line. The response should be *Hello Spring!*

The same applies to the "/helloworld" endpoint, which should respond with *Hello World!*.

## Extras 

### Makefile

This Project brings a Makefile that defines a set of steps to be executed, making it easier and very practical to build and run the application. Below are the possibilities:

|     Target    |                                      Action                                    |
|:-------------:|:------------------------------------------------------------------------------:|
|      all      | builds spring boot application, creates docker image and runs docker container |
|  gradle-build | builds spring boot application and generate jar file                           |
| build         | creates docker image from Dockerfile                                           |
| run           | runs docker container exposing port 8080                                       |
| stop          | stops docker container that is up                                              |
| rm            | removes stopped docker container                                               |
| rmi           | removes docker image from Dockerfile                                           |
| clean         | removes application container and docker image                                 |

To check how to run commands execute the `make` command or `make help`.

### GitlabCI Pipeline

This project has a GitlabCI pipeline with some stages to create a more automated environment. The pipeline executes stages for:

* Building the code and generating the jar file
* Making the quality analysis with Code Climate to generate gl-code-quality-report.json
* Building and pushing the container image with the application to DockerHub.

> **Note:** The job is executed every time that a new commit is made, but the stage to build and push a container image to DockerHub only runs when in the master branch.

## Contributing

If you want to contribute, please ensure your Merge Request follows to the guidelines below:

-  Be aware if you suggestion is not going to be a duplicate one by searching it before. 
-  Make an individual MR for each suggestion, keeping descriptions short and simple but yet descriptive.
-  New categories or improvements are always welcome.

Thank you for your suggestions!

## Notes

Following the requirements of the challenge the code was fixed and the Dockerfile and Makefile were created as well. As a plus, a Continuous Integration Pipeline was developed to show aditional DevOps skills.

There are a few improvements points:

* Speed up the Code Climate quality analysis execution, since it is taking too long to execute inside the pipeline and it is good practice to have a fast Continuous Integration flow.
* As it was a small project with one person, it was chosen to only have a master branch and all the development were done in separate branches that were then merged to the master. It could be a good practice to have at least a develop branch.
* Add Continuous Delivery steps inside the pipeline as an extra.

To solve the first problem, [Codebeat](https://codebeat.co/projects/gitlab-com-jullyannem-ob-teamday-challenge-master) was integrated to the project to also generate a code quality analysis. 